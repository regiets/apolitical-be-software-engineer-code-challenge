'use strict';

const uuid = require('uuid');

const MAX_VIEWS = 3;

function makeServices({store}) {
	async function requestView(userId) {
		if (!userId || typeof userId !== 'string' || !uuid.validate(userId))
			userId = uuid.v4();
		let session = await store.read(userId);
	
		if (!session)
			session = await store.create(userId, {count: 0});
	
		if (session.count < MAX_VIEWS)
			await store.update(userId, {count: session.count+1})
	
		return {
			userId,
			articlesViewed: session.count,
			hasAccess: session.count < MAX_VIEWS
		};
	}
	
	async function getViewCount(userId) {
		if (!userId) throw new Error('userId must be defined.');
		if (typeof userId !== 'string') throw new Error('userId must be a string.');
		if (!uuid.validate(userId)) throw new Error('userId must be be a valid id.');
		
		const session = await store.read(userId);
	
		if (!session) throw new Error('Could not find user for userId')
		
		return {
			userId,
			articlesViewed: session.count,
			hasAccess: session.count < MAX_VIEWS
		};
	}

	return {requestView, getViewCount};
}

module.exports = makeServices;
