const request = require('supertest');
const setCookieParser = require('set-cookie-parser');
const uuid = require('uuid');

const makeServer = require('./server');
const makeRoutes = require('../routes');
const makeServices = require('../services');
const makeStore = require('../loaders/store');

const logger = {log: () => {}};
const httpLogger = (req, res, next) => next();

describe('GET /article', () => {
	test('should respond with json', async done => {
		const store = await makeStore();
		const services = makeServices({store});
		const routes = makeRoutes({services});
		const server = makeServer({logger, httpLogger, routes});
		
		request(server)
			.get('/articles')
			.set('Accept', 'application/json')
      .expect(200)
      .then(response => {
          expect(typeof response.body.userId).toEqual('string')
          expect(uuid.validate(response.body.userId)).toBe(true)
          expect(response.body.accessGranted).toBe(true)
          expect(response.body.articlesViewed).toEqual(0)
          done();
      })
      .catch(err => done(err))
	});

	test('should respond with Set Cookie header', async done => {
		const store = await makeStore();
		const services = makeServices({store});
		const routes = makeRoutes({services});
		const server = makeServer({logger, httpLogger, routes});
		
		request(server)
			.get('/articles')
			.set('Accept', 'application/json')
      .expect(200)
			.then(setCookieParser)
      .then(cookies => {
				expect(cookies[0].name).toEqual('sid')
				expect(uuid.validate(cookies[0].value)).toBe(true)
				expect(cookies[0].path).toEqual('/')
				expect(cookies[0].httpOnly).toBe(true)
				done();
      })
      .catch(err => done(err))
	});

	test('should respond with incremented articlesViewed count', async done => {
		const store = await makeStore();
		const services = makeServices({store});
		const routes = makeRoutes({services});
		const server = makeServer({logger, httpLogger, routes});
		
		request(server)
			.get('/articles')
			.set('Accept', 'application/json')
			.then(response => {
				return request(server)
					.get('/articles')
					.set('Accept', 'application/json')
					.set('cookie', [`sid=${response.body.userId}`])
					.expect(200)
					.then(response2 => {
							expect(response2.body.userId).toEqual(response.body.userId)
							expect(uuid.validate(response2.body.userId)).toBe(true)
							expect(response2.body.accessGranted).toBe(true)
							expect(response2.body.articlesViewed).toEqual(1)
							done();
					})
					.catch(err => done(err))
			})

	});
});

describe('GET /users/:userId', () => {
	test('should respond with incremented articlesViewed count', async done => {
		const store = await makeStore();
		const services = makeServices({store});
		const routes = makeRoutes({services});
		const server = makeServer({logger, httpLogger, routes});
		
		request(server)
			.get('/articles')
			.set('Accept', 'application/json')
			.then(response => {
				// console.log(response.body)
				return request(server)
					.get(`/users/${response.body.userId}`)
					.set('Accept', 'application/json')
					.expect(200)
					.then(response2 => {
							expect(response2.body.userId).toEqual(response.body.userId)
							expect(uuid.validate(response2.body.userId)).toBe(true)
							expect(response2.body.hasAccess).toBe(true)
							expect(response2.body.articlesViewed).toEqual(1)
							done();
					})
					.catch(err => done(err))
			})

	});

	test('should respond with 404 for unknown userId', async done => {
		const store = await makeStore();
		const services = makeServices({store});
		const routes = makeRoutes({services});
		const server = makeServer({logger, httpLogger, routes});
		
		request(server)
			.get('/users/2b4f0613-f656-47fd-b1c4-1223c1033cfa')
			.set('Accept', 'application/json')
      .then(response => {
				expect(response.status).toEqual(404)
				done();
      })
      .catch(err => done(err))
	});
});
