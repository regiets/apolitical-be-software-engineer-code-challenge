'use strict';

const morgan = require('morgan');
const {createLogger, format, transports} = require('winston');
const {combine, timestamp, json, prettyPrint} = format;

const logger = createLogger({
	format: combine(
		timestamp(),
		json(),
		prettyPrint()
	),
	transports: [
		new transports.Console({
			level: 'debug'
		})
	]
});

logger.stream = {
	write: (message, encoding) => {
		const [method, url, status, _, __, time] = message.split(' ');
		logger.log({level: 'http', method, url, status, time});
	}
}

const httpLogger = morgan('tiny', {stream: logger.stream});


module.exports = {logger, httpLogger};
