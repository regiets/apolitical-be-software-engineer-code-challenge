'use strict';

const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const helmet = require('helmet');

function makeServer({logger, httpLogger, routes}) {
	const app = express();

	app.use(cors());
	app.use(helmet());
	app.use(httpLogger);
	app.use(cookieParser());
	
	app.use('/', routes);
	
	app.use((req, res, next) => {
		const err = new Error('Not Found');
		err.status = 404;
		next(err);
	});
	
	app.use((err, req, res, next) => {
		res.status(err.status || 500);
		logger.log({level: 'error', message: err.message || err.toString()})
		return res.json({
			error: err.status === 404 ? 'Not Found' : 'Internal Server Error'
		});
	});

	return app;
}

module.exports = makeServer;
