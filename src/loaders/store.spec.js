const makeStore = require('./store');

describe('makeStore()', () => {
	test('should resolve and return a "create" function', async done => {
		const store = await makeStore();

		expect(store.create).toBeDefined();
		expect({}.toString.call(store.create) === '[object Function]').toBe(true);
		done();
	});

	test('should resolve and return a "read" function', async done => {
		const store = await makeStore();

		expect(store.read).toBeDefined();
		expect({}.toString.call(store.read) === '[object Function]').toBe(true);
		done();
	});

	test('should resolve and return a "update" function', async done => {
		const store = await makeStore();

		expect(store.update).toBeDefined();
		expect({}.toString.call(store.update) === '[object Function]').toBe(true);
		done();
	});
});


describe('makeStore().create()', () => {
	test('should throw error if the id is not a string', async done => {
		const store = await makeStore({abc: {count: 0}});
		await expect(store.create(123, {})).rejects.toThrow();
		done();
	});

	test('should throw error if the obj is undefined', async done => {
		const store = await makeStore({abc: {count: 0}});
		await expect(store.create(123)).rejects.toThrow();
		done();
	});

	test('should throw error if the id already exists in store', async done => {
		const store = await makeStore({abc: {count: 0}});
		await expect(store.create("abc", {})).rejects.toThrow();
		done();
	});

	test('should return an object with property count = 2', async done => {
		const store = await makeStore({abc: {count: 0}});
		await expect(store.create("abd", {count: 2})).resolves.toEqual(expect.objectContaining({
			count: 2
		}));
		done()
	});
});


describe('makeStore().read()', () => {
	test('should throw error if the id is not a string', async done => {
		const store = await makeStore({abc: {count: 0}});
		await expect(store.read()).rejects.toThrow();
		done();
	});

	test('should return an object with property count = 0', async done => {
		const store = await makeStore({abc: {count: 0}});
		await expect(store.read("abc")).resolves.toEqual(expect.objectContaining({
			count: 0
		}));
		done();
	});

	test('should return undefined if id doesn\'t match a key', async done => {
		const store = await makeStore({abc: {count: 0}});
		const result = await store.read("xyz");
		expect(result).toBeUndefined();
		done();
	});
});


describe('makeStore().update()', () => {
	test('should throw error if the id is not a string', async done => {
		const store = await makeStore({abc: {count: 0}});
		await expect(store.update([], {})).rejects.toThrow();
		done();
	});

	test('should throw error if the obj is undefined', async done => {
		const store = await makeStore({abc: {count: 0}});
		await expect(store.update('abc')).rejects.toThrow();
		done();
	});

	test('should return an object with property count = 2', async done => {
		const store = await makeStore({abc: {count: 0}});
		await expect(store.update("abc", {count: 2})).resolves.toEqual(expect.objectContaining({
			count: 2
		}));
		done();
	});

	test('should return an object with property count = 2', async done => {
		const store = await makeStore({abc: {count: 0}});

		await expect(store.update("abd", {count: 2})).resolves.toEqual(expect.objectContaining({
			count: 2
		}));
		await expect(store.read("abc")).resolves.toEqual(expect.objectContaining({
			count: 0
		}));
		done();
	});
});
