function makeStore(initialValue = {}) {
	const store = initialValue;
	
	function create(id, obj) {
		return new Promise((resolve, reject) => {
			if (!id || typeof id !==  'string') return reject(new Error('"id" must be a string.'));
			if (!obj) return reject(new Error('"obj" must be defined.'));
			if (store[id]) return reject(new Error('Data for "id" already exists.'));
			
			store[id] = obj;
			return resolve(obj);
		});
	}
	
	function read(id) {
		return new Promise((resolve, reject) => {
			if (!id || typeof id !==  'string') return reject(new Error('"id" must be a string.'));
			return resolve(store[id]);
		});
	}
	
	function update(id, obj) {
		return new Promise((resolve, reject) => {
			if (!id || typeof id !==  'string') return reject(new Error('"id" must be a string.'));
			if (!obj) return reject(new Error('"obj" must be defined.'));
			
			const updatedValue = {...store[id], ...obj};
			store[id] = updatedValue;
			return resolve(updatedValue);
		});
	}

	return Promise.resolve({create, read, update});
}

module.exports = makeStore;
