'use strict';

const {Router} = require('express');

const SESSION_COOKIE = 'sid';

function makeRoutes({services}) {
	const router = Router();

	router.get('/articles', async (req, res, next) => {
		return services.requestView(req.cookies[SESSION_COOKIE])
			.then(({userId, articlesViewed, hasAccess}) => {
				res.cookie(SESSION_COOKIE, userId, {httpOnly: true});
				// console.log(userId)
				return res.json({
					userId,
					accessGranted: hasAccess,
					articlesViewed
				});
			})
			.catch((err) => {
				return next(err);
			});
	});
	
	router.get('/users/:userId', (req, res, next) => {
		return services.getViewCount(req.params.userId)
			.then(({articlesViewed, hasAccess, userId}) => {
				return res.json({
					userId,
					articlesViewed,
					hasAccess
				});
			})
			.catch((err) => {
				err.status = 404;
				next(err);
			});
	});

	return router;
}

module.exports = makeRoutes;

