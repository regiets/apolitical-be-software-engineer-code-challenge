'use strict';

const makeServer = require('./config/server');
const {logger, httpLogger} = require('./config/logger');
const makeRoutes = require('./routes');
const makeServices = require('./services');
const makeStore = require('./loaders/store');

const PORT = 8080;

makeStore()
	.then((store) => {
		const services = makeServices({store});
		const routes = makeRoutes({services});
		const server = makeServer({logger, httpLogger, routes});
		
		server.listen(PORT, () => {
			logger.log({
				level: 'info',
				message: `Server started.`,
				port: PORT
			});
		});
	});

