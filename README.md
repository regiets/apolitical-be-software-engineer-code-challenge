# BE coding challenge

### API
This API exposes two routes and is designed to be accessed from any client/server to check how many articles a given user has read and to prevent any user from reading more than 3 articles.

##### GET `/articles`
This endpoint allows a client to check if they are allowed to access a new article. If the client does not supply a `sid=<userId>` Cookie header or the `userId` is malformed, a new `userId` is generated and the article count is set to 0. The `userId` is then returned to the client in a `Set-Cookie` header, as well as part of the response body. Along with the `userId`, the response body retuns the current count of viewed articles (not including the one requested to be viewed) as `articlesViewed` and a `accessGranted` boolean value, indicating if the user is allowed to access the requested article.

**Request**
```
General:
  method: GET
  url: /articles
Headers (optional):
  Cookie: 'sid=<userId>'
```
**Response**
```
Body:
	userId: <userId>
	accessGranted: <boolean>
	articlesViewed: <integer>
Headers:
  Set-Cookie: 'sid=b5c336d5-1fbe-4495-8813-f7b076a50495; Path=/; HttpOnly'
```

##### GET `/users/:userId`
This endpoint allows a client to check how many articles a particular user has viewed. The response contains the `userId`, the current count of viewed articles as `articlesViewed` and a `hasAccess` boolean value, indicating if the user is allowed to access more articles.

**Request**
```
General:
  method: GET
  url: /users/<userId>
```
**Response**
```
Body:
	userId: <userId>
	hasAccess: <boolean>
	articlesViewed: <integer>
```


### Running & testing the application locally

##### Testing
`jest` is used as the test runner for this application in combination with `supertest` to test the server endpoints. Use the below npm script to run all tests.
```sh
$ npm run test
```

##### Development
`nodemon` is used to run the application locally during development, allowing the server to restart after every saved change.
```sh
$ npm run start:dev
```
The API can be accessed at:
```
http://localhost:8080/
```

##### Compiled production build
`webpack` is used to compile the server. Use the below npm script to compile and run the server.
```sh
$ npm run run:dist
```
The API can be accessed at:
```
http://localhost:8080/
```

##### Docker build
A multi stage docker build is used to minimize the docker image size for production. The below npm script can be used to compile the javascript, build the docker image and run it locally on port `49160`.
```sh
$ npm run run:docker
```
The API can be accessed at:
```
http://localhost:49160/
```

### Scalability Strategy

##### Scaling to millions of users
1. As dependency injection is used throughout the codebase, the in-memory store is completely decoupled and can be swapped for any kind of database (i.e. Redis/MongoDB/PostgreSQL). It only requires the three exposed functions to be implemented for a particular database.
2. Implement a health/readiness probe (route or function) that can be used to check the health of the application.
3. Implement a shutdown function that runs when the nodeJS process receives the SIGTERM event, so the resources can be terminated gracefully.
4. The docker container can then be run in pods of a Kubernetes cluster or similar service that supports auto scaling.
5. Finally the service can then be exposed through an API Gateway/Ingress controller (i.e. NGINX) on a static IP address to the public internet.

##### Logging and monitoring at scale
1. Add a new transport (storage device) to the logger configuration file.
2. Include a reference to the ID of the kubernetes pod in logs.
3. Stream logs of individual kubernetes pods to a central place such as Cloud Logging of the GCP's operations suite (formerly Stackdriver).
4. Monitor the health and collect logs and uptime metrics of the individual services with Cloud Monitoring of the GCP's operations suite.

### Improvements
1. Feature: Add a `/articles/:articleId` route to keep track of the read articles, so a user can keep accessing the 3 articles.
2. Performance: Remove prettyprint json logs for production builds using environment variables.
3. Tech Debt: Clearly separate article and user services in services directory.
