const path = require('path')

module.exports = {
	rootDir: path.join(__dirname, '.'),
	verbose: false,
	moduleFileExtensions: [
		'js',
		'json'
	],
	testPathIgnorePatterns: [
		'/node_modules/',
		'/build/'
	],
	// collectCoverage: true,
	testEnvironment: 'node',
	testMatch: [
		'**/*.spec.js'
	],
};