# Specify base image
FROM node:14-alpine as build

# Set working directory
WORKDIR /usr/src/app

# Copy required files into container
COPY package*.json ./
COPY ./dist ./

# Install app dependencies
RUN npm ci --only=production

# Create new docker container
FROM node:14-alpine

# Set working directory
WORKDIR /usr/src/app

# Copy files including nodejs dependencies from build container (no npm dependencies)
COPY --from=build /usr/src/app/node_modules node_modules
COPY --from=build /usr/src/app/server.js server.js

# Set non-root user provided by node image
USER node

# Expose on port 8080
EXPOSE 8080

# Set start command
CMD [ "node", "server.js" ]
