const path = require('path')
const nodeExternals = require('webpack-node-externals')

module.exports = {
	mode: 'production',
  entry: './src/app.js',
	target: 'node',
  output: {
    filename: 'server.js',
    path: path.resolve(__dirname, 'dist')
  },
	node: {
    __dirname: false,
    __filename: false,
  },
  externals: [nodeExternals()],
}
